﻿using System;
using System.Collections.Generic;
using NLog;
using PhotoUploader.Application;
using PhotoUploader.Application.Application.Concrete;
using PhotoUploader.Application.Application.Interfaces;
using PhotoUploader.Application.Configuration;
using PhotoUploader.Application.Services.VKService;

namespace PhotoUploader.Console
{
    class Program
    {
        private static PhotoSenderApplication _senderApplication;

        static void Main(string[] args)
        {
            ConsoleDisplayHelper.InfoMessage($"{DateTime.Now} - Application Starting...");

            try
            {
                Settings settings = new Settings(Settings.DefaultSettingsFileName);

                IList<IPostingStrategy> strategies = new List<IPostingStrategy>
                {
                    new VKontaktePostingStategy(new VkService(new VkServiceSettings()
                    {
                        ApplicationId = settings.ApplicationId,
                        Login = settings.VkLogin,
                        Password = settings.VkPassword,
                        GroupId = settings.GroupId,
                        AlbumName = settings.AlbumName
                    }))
                };

                ILogger logger = LogManager.GetLogger("AppLogger");
                _senderApplication = new PhotoSenderApplication(logger, settings, strategies);
            }
            catch (Exception e)
            {
                ConsoleDisplayHelper.ErrorMessage($"{DateTime.Now} - {e}");
                ConsoleDisplayHelper.WarningMessage("Press any key to exit...", true);
                return;
            }

            try
            {
                if (_senderApplication.IsInitialized)
                {
                    _senderApplication.Start();
                }
                else
                {
                    ConsoleDisplayHelper.ErrorMessage($"{DateTime.Now} - Application failed to initialize.");
                }
            }
            catch (Exception e)
            {
                ConsoleDisplayHelper.ErrorMessage($"{DateTime.Now} - {e}");
                ConsoleDisplayHelper.WarningMessage("Press any key to exit...", true);
                return;
            }

            ConsoleDisplayHelper.InfoMessage($"{DateTime.Now} - Application Finished.");
            ConsoleDisplayHelper.InfoMessage("Press any key to exit...", true);

            System.Console.ReadKey();
        }
    }
}
