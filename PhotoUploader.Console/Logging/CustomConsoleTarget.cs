﻿using NLog;
using NLog.Targets;

namespace PhotoUploader.Console.Logging
{
    [Target("CustomConsole")]
    public sealed class CustomConsoleTarget: TargetWithLayout
    {
        protected override void Write(LogEventInfo logEvent)
        {
            string message = this.Layout.Render(logEvent);

            if (logEvent.Level == LogLevel.Error)
            {
                ConsoleDisplayHelper.ErrorMessage(message);
            }
            else if (logEvent.Level == LogLevel.Warn)
            {
                ConsoleDisplayHelper.WarningMessage(message);
            }
            else if (logEvent.Level == LogLevel.Info)
            {
                ConsoleDisplayHelper.InfoMessage(message);
            }
            else
            {
                ConsoleDisplayHelper.DefaultMessage(message);
            }
        }
    }
}
