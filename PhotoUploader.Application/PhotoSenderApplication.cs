﻿using System;
using System.Collections.Generic;
using NLog;
using PhotoUploader.Application.Application.Concrete;
using PhotoUploader.Application.Application.Interfaces;
using PhotoUploader.Application.Application.Models;
using PhotoUploader.Application.Configuration;

namespace PhotoUploader.Application
{
    public class PhotoSenderApplication
    {
        private ILogger Logger { get; }

        public Settings Settings { get; }

        public bool IsInitialized { get; }

        private IList<IPostingStrategy> PostingStrategies { get; }
        private IPhotoScanner PhotoScanner { get; }
        private IPhotoLoader PhotoLoader { get; }

        public PhotoSenderApplication(ILogger logger, Settings settings, IList<IPostingStrategy> postingStrategies)
        {
            this.Logger = logger;
            this.Settings = settings;
            this.PostingStrategies = postingStrategies;

            try
            {
                this.PhotoScanner = new DirectoryPhotoScanner(this.Settings.PhotosSourcePath);
                this.PhotoLoader = new DirectoryPhotoLoader(this.Settings.ProcessedPhotosPathOnSuccsess, this.Settings.ProcessedPhotosPathOnFailure);

                this.IsInitialized = true;
            }
            catch (Exception e)
            {
                this.IsInitialized = false;
                this.Logger.Error($"PhotoSender initialization failed. Details: {e}");
            }
        }

        public void Start()
        {
            //TODO: Implement cyclic work with delay

            this.DoWork();
        }

        public void Stop()
        {
            //TODO: Implement stop command
        }

        private void DoWork()
        {
            this.Logger.Warn($"PhotoSender started.");

            if (!this.IsInitialized)
            {
                this.Logger.Warn($"Attempt to start PhotoSender before initialization.");
                return;
            }

            this.Logger.Info($"Searching photos into \'{this.Settings.PhotosSourcePath}\'...");

            IList<Photo> photos = this.PhotoScanner.ScanPhotos();

            this.Logger.Warn($"[{photos.Count}] photos detected.");

            int i = 0;
            foreach (Photo photo in photos)
            {
                ++i;
                this.Logger.Info($"({i}) Start of \'{photo.FileName}\' photo processing.");

                bool succeeded = true;
                foreach (IPostingStrategy postStarteStrategy in this.PostingStrategies)
                {
                    this.Logger.Debug($"\t\t Uploading to \'{postStarteStrategy.StrategyName}\' started...");

                    try
                    {
                        postStarteStrategy.Upload(photo);
                        this.Logger.Debug($"\t\t Uploading to \'{postStarteStrategy.StrategyName}\' finished.");
                    }
                    catch (Exception e)
                    {
                        succeeded = false;
                        this.Logger.Error($"Photo \'{photo.FileName}\' uploading to \'{postStarteStrategy.StrategyName}\' failed. Details: {e}");
                    }
                }

                try
                {
                    if (succeeded)
                    {
                        this.PhotoLoader.MovePhotoToDirectoryOnSuccsess(photo);
                        this.Logger.Debug($"\t\t Moved to the \'{this.Settings.ProcessedPhotosPathOnSuccsess}\' directory.");
                    }
                    else
                    {
                        this.PhotoLoader.MovePhotoToDirectoryOnFailure(photo);
                        this.Logger.Debug($"\t\t Moved to the \'{this.Settings.ProcessedPhotosPathOnFailure}\' directory.");
                    }
                }
                catch (Exception e)
                {
                    string directoryToStore = succeeded ? this.Settings.ProcessedPhotosPathOnSuccsess : this.Settings.ProcessedPhotosPathOnFailure;
                    this.Logger.Error($"Moving photo \'{photo.Path}\' to the \'{directoryToStore}\' directory failed. Details: {e}");
                }

                if (succeeded)
                {
                    this.Logger.Info($"({i}) Successfully finished processing \'{photo.FileName}\' photo.");
                }
                else
                {
                    this.Logger.Warn($"({i}) Finished. There were troubles during processing \'{photo.FileName}\' photo.");
                }
            }

            this.Logger.Warn($"PhotoSender finished.");
        }
    }
}
