﻿using PhotoUploader.Application.Application.Models;

namespace PhotoUploader.Application.Application.Interfaces
{
    public interface IPhotoLoader
    {
        void MovePhotoToDirectoryOnSuccsess(Photo photoToMove);

        void MovePhotoToDirectoryOnFailure(Photo photoToMove);
    }
}