﻿using PhotoUploader.Application.Application.Models;

namespace PhotoUploader.Application.Application.Interfaces
{
    public interface IPostingStrategy
    {
        string StrategyName { get; }

        void Upload(Photo photo);
    }
}
