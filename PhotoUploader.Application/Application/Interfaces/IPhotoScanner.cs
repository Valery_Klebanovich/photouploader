﻿using System.Collections.Generic;
using PhotoUploader.Application.Application.Models;

namespace PhotoUploader.Application.Application.Interfaces
{
    public interface IPhotoScanner
    {
        IList<Photo> ScanPhotos();
    }
}