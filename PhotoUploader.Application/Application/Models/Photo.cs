﻿using PhotoUploader.Validators;

namespace PhotoUploader.Application.Application.Models
{
    public class Photo
    {
        public Photo(string photoPath)
        {
            Guard.ArgumentNotNullOrEmpty(photoPath, "photoPath");

            this.Path = photoPath;
        }

        public string FileName => System.IO.Path.GetFileName(this.Path);

        public string Path { get; set; }
    }
}
