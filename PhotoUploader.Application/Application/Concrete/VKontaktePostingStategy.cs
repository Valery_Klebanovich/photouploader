﻿using PhotoUploader.Application.Application.Interfaces;
using PhotoUploader.Application.Application.Models;
using PhotoUploader.Application.Services.VKService;
using PhotoUploader.Validators;

namespace PhotoUploader.Application.Application.Concrete
{
    public class VKontaktePostingStategy: IPostingStrategy
    {
        private VkService VkService { get; }

        public string StrategyName => "VKontakte";

        public VKontaktePostingStategy(VkService vkService)
        {
            Guard.ArgumentNotNull(vkService, "vkService");

            this.VkService = vkService;
        }

        public void Upload(Photo photo)
        {
            this.VkService.PostImage(photo);
        }
    }
}
