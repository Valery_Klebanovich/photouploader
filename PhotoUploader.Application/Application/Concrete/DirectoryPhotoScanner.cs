﻿using System;
using System.Collections.Generic;
using System.Linq;
using PhotoUploader.Application.Application.Interfaces;
using PhotoUploader.Application.Application.Models;
using PhotoUploader.Application.Utilities;
using PhotoUploader.Validators;

namespace PhotoUploader.Application.Application.Concrete
{
    public class DirectoryPhotoScanner : IPhotoScanner
    {
        public string ScannedDirectoryPath { get; }

        public DirectoryPhotoScanner(string scannedDirectoryPath)
        {
            Guard.ArgumentNotNullOrEmpty(scannedDirectoryPath, "scannedDirectoryPath");

            this.ScannedDirectoryPath = scannedDirectoryPath;

            if (!DirectoryManager.IsDirectoryExists(this.ScannedDirectoryPath))
            {
                throw new Exception($"Scanned directory is not exist: \'{this.ScannedDirectoryPath}\'");
            }
        }

        public IList<Photo> ScanPhotos()
        {
            IList<string> files = DirectoryManager.GetPhotosFromDirectory(this.ScannedDirectoryPath);

            return files.Select(f => new Photo(f)).ToList();
        }
    }
}
