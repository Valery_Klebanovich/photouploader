﻿using System;
using PhotoUploader.Application.Application.Interfaces;
using PhotoUploader.Application.Application.Models;
using PhotoUploader.Application.Utilities;
using PhotoUploader.Validators;

namespace PhotoUploader.Application.Application.Concrete
{
    public class DirectoryPhotoLoader : IPhotoLoader
    {
        public string DirectoryPathOnSuccsess { get; }
        public string DirectoryPathOnFailure { get; }

        public DirectoryPhotoLoader(string directoryPathOnSuccsess, string directoryPathOnFailure)
        {
            Guard.ArgumentNotNullOrEmpty(directoryPathOnSuccsess, "directoryPathOnSuccsess");
            Guard.ArgumentNotNullOrEmpty(directoryPathOnFailure, "directoryPathOnFailure");

            this.DirectoryPathOnSuccsess = directoryPathOnSuccsess;
            this.DirectoryPathOnFailure = directoryPathOnFailure;

            DirectoryManager.CreateDirectory(this.DirectoryPathOnSuccsess);
            DirectoryManager.CreateDirectory(this.DirectoryPathOnFailure);
        }

        public void MovePhotoToDirectoryOnSuccsess(Photo photoToMove)
        {
            Guard.ArgumentNotNull(photoToMove, "photoToMove");

            this.MovePhotoToDirectory(this.DirectoryPathOnSuccsess, photoToMove);
        }

        public void MovePhotoToDirectoryOnFailure(Photo photoToMove)
        {
            Guard.ArgumentNotNull(photoToMove, "photoToMove");

            this.MovePhotoToDirectory(this.DirectoryPathOnFailure, photoToMove);
        }

        private void MovePhotoToDirectory(string directpryPath, Photo photoToMove)
        {
            try
            {
                DirectoryManager.MoveFileToDirectory(directpryPath, photoToMove.Path);
            }
            catch (Exception e)
            {
                throw new Exception(
                    $"Failed move photo \'{photoToMove.Path}\' to directory \'{directpryPath}\'. Reason: {e.Message}");
            }
        }
    }
}
