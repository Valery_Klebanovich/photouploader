﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using PhotoUploader.Validators;

namespace PhotoUploader.Application.Utilities
{
    public static class DirectoryManager
    {
        public static IList<string> GetPhotosFromDirectory(string directoryPath)
        {
            Guard.ArgumentNotNullOrEmpty(directoryPath, "directoryPath");

            IList<string> resultFiles = new List<string>();

            string[] files = Directory.GetFiles(directoryPath);
            foreach (string file in files)
            {
                try
                {
                    DirectoryManager.PhotoCheckup(file);

                    resultFiles.Add(file);
                }
                catch (Exception)
                {
                    //NOTE: do nothing in case of the file is not an image
                }
            }

            return resultFiles;
        }

        public static bool IsDirectoryExists(string directoryPath)
        {
            Guard.ArgumentNotNullOrEmpty(directoryPath, "directoryPath");

            return Directory.Exists(directoryPath);
        }

        public static void CreateDirectory(string directoryPath)
        {
            Guard.ArgumentNotNullOrEmpty(directoryPath, "directoryPath");

            if (!DirectoryManager.IsDirectoryExists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
        }

        public static void MoveFileToDirectory(string targetDirectoryPath, string photoPath)
        {
            Guard.ArgumentNotNullOrEmpty(targetDirectoryPath, "targetDirectoryPath");
            Guard.ArgumentNotNullOrEmpty(photoPath, "photoPath");

            File.Move(photoPath, targetDirectoryPath + Path.DirectorySeparatorChar + Path.GetFileName(photoPath));
        }

        private static void PhotoCheckup(string file)
        {
            using (Image image = Image.FromFile(file))
            {
                using (Graphics graphics = Graphics.FromImage(image))
                {
                    ImageFormat imageFormat = image.RawFormat;
                }
            }
        }
    }
}
