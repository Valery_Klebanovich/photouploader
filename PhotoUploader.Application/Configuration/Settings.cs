﻿using System;
using System.Globalization;
using System.IO;
using IniParser;
using IniParser.Model;
using PhotoUploader.Validators;

namespace PhotoUploader.Application.Configuration
{
    public sealed class Settings
    {
        public const string DefaultSettingsFileName = "Configuration.ini";

        private const string VkSection = "VK";
        private const string PhotosRoutesSection = "PhotosRoutes";

        public Settings(string settingsFilePath)
        {
            if (!File.Exists(settingsFilePath))
            {
                throw new FileNotFoundException("Configuration.ini file was not found.");
            }

            FileIniDataParser parser = new FileIniDataParser();
            IniData data = parser.ReadFile(settingsFilePath);

            this.ApplicationId = ulong.Parse(data[Settings.VkSection]["ApplicationId"]);

            this.VkLogin = data[Settings.VkSection]["Login"];
            Guard.ArgumentNotNullOrEmpty(this.VkLogin, "Login");

            this.VkPassword = data[Settings.VkSection]["Password"];
            Guard.ArgumentNotNullOrEmpty(this.VkPassword, "Password");

            this.GroupId = data[Settings.VkSection]["GroupId"];
            Guard.ArgumentNotNullOrEmpty(this.GroupId, "GroupId");

            this.AlbumName = data[Settings.VkSection]["AlbumName"];
            if (string.IsNullOrWhiteSpace(this.AlbumName))
            {
                this.AlbumName = DateTime.Now.ToString("dd-MM-yyyy", CultureInfo.InvariantCulture);
            }

            this.PhotosSourcePath = data[Settings.PhotosRoutesSection]["PhotosSourcePath"];
            Guard.ArgumentNotNullOrEmpty(this.PhotosSourcePath, "PhotosSourcePath");

            this.ProcessedPhotosPathOnSuccsess = data[Settings.PhotosRoutesSection]["ProcessedPhotosPathOnSuccsess"];
            Guard.ArgumentNotNullOrEmpty(this.ProcessedPhotosPathOnSuccsess, "ProcessedPhotosPathOnSuccsess");

            this.ProcessedPhotosPathOnFailure = data[Settings.PhotosRoutesSection]["ProcessedPhotosPathOnFailure"];
            Guard.ArgumentNotNullOrEmpty(this.ProcessedPhotosPathOnSuccsess, "ProcessedPhotosPathOnFailure");
        }

        #region Settings 

        public ulong ApplicationId { get; }

        public string VkLogin { get; }

        public string VkPassword { get; }

        public string GroupId { get; }

        public string AlbumName { get; }

        public string PhotosSourcePath { get; }

        public string ProcessedPhotosPathOnSuccsess { get; }

        public string ProcessedPhotosPathOnFailure { get; }

        #endregion
    }
}
