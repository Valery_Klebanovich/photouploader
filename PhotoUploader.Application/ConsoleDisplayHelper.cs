﻿using System;

namespace PhotoUploader.Application
{
    public static class ConsoleDisplayHelper
    {
        public static void InfoMessage(string message, bool waitKeyPressed = false)
        {
            ConsoleDisplayHelper.ShowMessage(ConsoleColor.Green, message, waitKeyPressed);
        }

        public static void WarningMessage(string message, bool waitKeyPressed = false)
        {
            ConsoleDisplayHelper.ShowMessage(ConsoleColor.Yellow, message, waitKeyPressed);
        }

        public static void ErrorMessage(string message, bool waitKeyPressed = false)
        {
            ConsoleDisplayHelper.ShowMessage(ConsoleColor.Red, message, waitKeyPressed);
        }

        public static void DefaultMessage(string message, bool waitKeyPressed = false)
        {
            ConsoleDisplayHelper.ShowMessage(ConsoleColor.White, message, waitKeyPressed);
        }

        private static void ShowMessage(ConsoleColor color, string message, bool waitKeyPressed)
        {
            System.Console.ForegroundColor = color;
            System.Console.WriteLine(message);

            if (waitKeyPressed)
            {
                System.Console.ReadKey();
            }
        }
    }
}
