﻿using PhotoUploader.Application.Application.Models;
using PhotoUploader.Validators;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VkNet;
using VkNet.Model;
using VkNet.Model.RequestParams;
using VK = VkNet.Enums.Filters;

namespace PhotoUploader.Application.Services.VKService
{
    public class VkService
    {
        private VkServiceSettings Settings { get; }

        public VkService(VkServiceSettings settings)
        {
            Guard.ArgumentNotNull(settings, "settings");
            this.Settings = settings;
        }

        public void PostImage(Photo photo)
        {
            Guard.ArgumentNotNull(photo, "photo");

            // Authorization 
            VkApi api = this.GetApi();
            
            // Get Target Group
            Group group = this.GetTargetGroup(api);
            
            // Check whether target album exists
            PhotoAlbum album = this.GetTargetAlbum(api, group);

            if (album == null)
            {
                album = this.CreateTargetAlbum(api, group);
            }

            // Upload photo to the album
            this.UploadPhotoToAlbum(api, album, group, photo);
        }

        private VkApi GetApi()
        {
            // Авторизация
            ApiAuthParams authParams = new ApiAuthParams()
            {
                ApplicationId = this.Settings.ApplicationId,
                Login = this.Settings.Login,
                Password = this.Settings.Password,
                Settings = VK.Settings.All
            };

            // авторизуемся
            VkApi api = new VkApi();
            api.Authorize(authParams);

            return api;
        }

        private Group GetTargetGroup(VkApi api)
        {
            // Получаем группу по айди
            return api.Groups.GetById(new List<string>() { this.Settings.GroupId }, this.Settings.GroupId, VK.GroupsFields.All).FirstOrDefault();
        }

        private PhotoAlbum GetTargetAlbum(VkApi api, Group group)
        {
            PhotoGetAlbumsParams parameters = new PhotoGetAlbumsParams()
            {
                OwnerId = -group.Id
            };

            var albums = api.Photo.GetAlbums(parameters);
            PhotoAlbum album = albums.FirstOrDefault(a => a.Title == this.Settings.AlbumName);

            return album;
        }

        private PhotoAlbum CreateTargetAlbum(VkApi api, Group group)
        {
            PhotoCreateAlbumParams parameters = new PhotoCreateAlbumParams()
            {
                GroupId = group.Id,
                Title = this.Settings.AlbumName
            };

            PhotoAlbum album = api.Photo.CreateAlbum(parameters);

            return album;
        }

        private void UploadPhotoToAlbum(VkApi api, PhotoAlbum album, Group group, Photo photo)
        {
            // Получить адрес сервера для загрузки.
            UploadServerInfo uploadServer = api.Photo.GetUploadServer(album.Id, group.Id);

            // Загрузить файл.
            System.Net.WebClient wc = new System.Net.WebClient();
            string saveFileResponse = Encoding.ASCII.GetString(wc.UploadFile(uploadServer.UploadUrl, photo.Path));

            // Сохранить загруженный файл
            PhotoSaveParams parameters = new PhotoSaveParams()
            {
                GroupId = group.Id,
                AlbumId = album.Id,
                SaveFileResponse = saveFileResponse,
                Caption = photo.FileName
            };

            api.Photo.Save(parameters);
        }
    }
}
