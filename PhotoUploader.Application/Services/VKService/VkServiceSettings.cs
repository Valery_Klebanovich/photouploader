﻿namespace PhotoUploader.Application.Services.VKService
{
    public class VkServiceSettings
    {
        public ulong ApplicationId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string GroupId { get; set; }
        public string AlbumName { get; set; }
    }
}
